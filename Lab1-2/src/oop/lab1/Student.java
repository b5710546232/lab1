package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a Student with a name and id.
 * 
 * @author Nattapat Sukpootanan
 */
public class Student extends Person {
	/** the Student's id. may contain spaces. */
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param id is the id of the new Student  and name is the name of the new Student.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/** 
	 * Compare Student's by id.
	 * They are equal if the id matches.
	 * @param other is another Student to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj==null) return false;
		if(obj.getClass()!=this.getClass()) return false;
		 Student other = (Student) obj;
		     if ( this.id==other.id ) 
		          return true;
		     return false;
	}
}
